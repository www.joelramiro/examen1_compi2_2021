#ifndef _AST_H_
#define _AST_H_

#include <list>
#include <string>

using namespace std;
class Expr;
class Parameter;
class Statement;
typedef list<Statement *> StatementList;
typedef list<Expr *> ArgumentList;
typedef list<Parameter *> ParameterList;
enum StatementKind{
    WHILE_STATEMENT,
    CALL_STATEMENT,
    EXPRESSION_STATEMENT,
    VARIABLE_STATEMENT,
    METHOD_DEFINITION_STATEMENT,
    BLOCK_STATEMENT,
    MAIN_STATEMENT
};

enum Type{
    INVALID,
    STRING,
    FLOAT,
    ASSIGN,
    BOOL
};

class Expr{
    public:
        int line;
        virtual float evaluate() = 0;
        virtual Type getType() = 0;
};

class Parameter{
    public:
        Parameter(string id, int line){
            this->id = id;
            this->line = line;
        }
        string id;
        int line;
        float evaluate();
};


class Statement{
    public:
        int line;
        virtual float evaluate() = 0;
        virtual StatementKind getKind() = 0;
};

class WhileStatement : public Statement{
    public:
        WhileStatement(Statement * conditionalExpr, Statement * Statement, int line){
            this->conditionalExpr = conditionalExpr;
            this->Statement = Statement;
            this->line = line;
        }
        Statement * conditionalExpr;
        Statement * Statement;
        float evaluate();
        StatementKind getKind(){return WHILE_STATEMENT;}
};


class MainStatement : public Statement{
    public:
        MainStatement(Statement * Statement, int line){
            this->Statement = Statement;
            this->line = line;
        }
        Statement * Statement;
        StatementKind getKind(){return MAIN_STATEMENT;}
};

class CallStatement : public Statement{
    public:
        CallStatement(string method, ArgumentList parameters, int line){
            this->method = method;
            this->parameters = parameters;
            this->line = line;
        }
        string method;
        ArgumentList parameters;
        int line;
        float evaluate();
        StatementKind getKind(){return CALL_STATEMENT;}
};

class ExprStatement : public Statement{
    public:
        ExprStatement(Expr * expr, int line){
            this->expr = expr;
            this->line = line;
        }
        Expr * expr;
        float evaluate();
        StatementKind getKind(){return EXPRESSION_STATEMENT;}
};


class BlockStatement : public Statement{
    public:
        BlockStatement(StatementList statements, int line){
            this->statements = statements;
            this->line = line;
        }
        StatementList statements;
        int line;
        float evaluate();
        StatementKind getKind(){
            return BLOCK_STATEMENT;
        }
};

class VariableStatement : public Statement{
    public:
        VariableStatement(string id, Expr * expr, int line){
            this->id = id;
            this->expr = expr;
            this->line = line;
        }
        string id;
        Expr * expr;
        float evaluate();
        StatementKind getKind(){return VARIABLE_STATEMENT;}
};


class BinaryExpr : public Expr{
    public:
        BinaryExpr(Expr * expr1, Expr *expr2, int line){
            this->expr1 = expr1;
            this->expr2 = expr2;
            this->line = line;
        }
        
        virtual float evaluate() = 0;
        virtual Type getType() = 0;
        Expr * expr1;
        Expr *expr2;
        int line;
};

class AssignExpr : public Expr{
    public:
        AssignExpr(Expr * expr1, Expr *expr2, int line){
            this->expr1 = expr1;
            this->expr2 = expr2;
            this->line = line;
        }
        Expr * expr1;
        Expr *expr2;
        int line;
        float evaluate();
        Type getType();
};

class MethodDefinition : public Statement{
    public:
        MethodDefinition(string id, ParameterList params, Statement * statement, int line){
            this->id = id;
            this->params = params;
            this->statement = statement;
            this->line = line;
        }

        string id;
        ParameterList params;
        Statement * statement;
        int line;
        float evaluate();
        StatementKind getKind(){
            return METHOD_DEFINITION_STATEMENT;
        }
};

class IdExpr : public Expr{
    public:
        IdExpr(string id, int line){
            this->id = id;
            this->line = line;
        }
        string id;
        int line;
        float evaluate();
        Type getType();
};


class FloatExpr : public Expr{
    public:
        FloatExpr(float value, int line){
            this->value = value;
            this->line = line;
        }
        float value;
        float evaluate();
        Type getType();
};





class SubExpr: public BinaryExpr{
    public:
        SubExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};

class MulExpr: public BinaryExpr{
    public:
        MulExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};

class DivExpr: public BinaryExpr{
    public:
        DivExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};

class GTExpr: public BinaryExpr{
    public:
        GTExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};

class LTExpr: public BinaryExpr{
    public:
        LTExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};

class AddExpr: public BinaryExpr{
    public:
        AddExpr(Expr * expr1, Expr * expr2, int yylineno): BinaryExpr(expr1, expr2,yylineno){}
        float evaluate();
        Type getType();
};
#endif