#include "ast.h"
#include <map>
#include <iostream>

class FunctionInfo;
map<string, float> variables;
map<string, FunctionInfo*> methods;

bool variableExists(string id){
  if(variables[id] != 0)
    return true;
  return false;
}

class FunctionInfo{
    public:
        list<Parameter *> parameters;
};

void addMethodDeclaration(string id, int line, ParameterList params){
    if(methods[id] != 0){
        cout<<"redefinition of function "<<id<<" in line: "<<line<<endl;
        exit(0);
    }
    methods[id] = new FunctionInfo();
    methods[id]->parameters = params;
}

float  MethodDefinition::evaluate(){
    if(this->params.size() > 4){
        cout<< "Method: "<<this->id << " can't have more than 4 parameters, line: "<< this->line<<endl;
        exit(0);
    }

    addMethodDeclaration(this->id, this->line, this->params);
 
    list<Parameter* >::iterator it = this->params.begin();
    while(it != this->params.end()){
        (*it)->evaluate();
        it++;
    }

    if(this->statement !=NULL ){
        this->statement->evaluate();
    }
    return 0;
}

float  Parameter::evaluate(){
    if(!variableExists(this->id)){
        variables[id] = 0;
    }else{
        cout<<"error: redefinition of variable: "<< this->id<< " line: "<<this->line <<endl;
        exit(0);
    }
    return 0;
}

float  WhileStatement::evaluate(){
  
  if (this->Statement != NULL)
  {
      this->Statement->evaluate();
  }
  
    return 0;   
}

float  CallStatement::evaluate(){
    return 0;   
    if(methods[this->method] == 0)
    {
        cout<<"error:The method not found: "<< this->method<< " line: "<<this->line <<endl;
        exit(0);
    }

    // ejecutar metodo
}



float  ExprStatement::evaluate(){
    return 0;   
}


float  BlockStatement::evaluate(){
    list<Statement *>::iterator its = this->statements.begin();
    while (its != this->statements.end())
    {
        Statement * stmt = *its;
        if(stmt != NULL){
            stmt->evaluate();
        }

        its++;
    }
    return 0;   
}


float  VariableStatement::evaluate(){
    if(variableExists(this->id))
    {
        cout<<"error: redefinition of variable: "<< this->id<< " line: "<<this->line <<endl;
        exit(0);   
    }
    if(this->expr == NULL)
    {
        variables[this->id] = 0;
    }
    else if (this->expr->getType() != FLOAT)
    {
        cout<<"error: invalid assigned type."<< " line: "<<this->line <<endl;
    }
    else
    {
        FloatExpr* c = dynamic_cast<FloatExpr*>(expr);
        variables[this->id] = c->value;
    }
    return 0;   
}

float AssignExpr::evaluate()
{
    return 0;
}

float FloatExpr::evaluate(){
    return this->value;
}

float AddExpr::evaluate(){
    return this->expr1->evaluate() + this->expr2->evaluate();
}

float SubExpr::evaluate(){
    return this->expr1->evaluate() - this->expr2->evaluate();
}

float MulExpr::evaluate(){
    return this->expr1->evaluate() * this->expr2->evaluate();
}

float DivExpr::evaluate(){
    float result = this->expr2->evaluate();
    if( result == 0)    
    {
        cout<<"error: Divission by zero is not posibble."<< " line: "<<this->line <<endl;
    }
    return this->expr1->evaluate() / result;
}

float GTExpr::evaluate(){
    return this->expr1->evaluate() > this->expr2->evaluate();
}

float LTExpr::evaluate(){
    return this->expr1->evaluate() < this->expr2->evaluate();
}

float IdExpr::evaluate(){
    if (!variableExists(this->id))
    {
        cout<<"Variable not found."<< " line: "<<this->line <<endl;
        exit(0);   
    }
    
    return  variables[this->id];
}

Type IdExpr::getType()
{
    return FLOAT;
}

Type FloatExpr::getType()
{
    return FLOAT;
}


Type AddExpr::getType()
{
    return FLOAT;
}

Type AssignExpr::getType()
{
    return FLOAT;
}


Type SubExpr::getType()
{
    return FLOAT;
}


Type MulExpr::getType()
{
    return FLOAT;
}


Type DivExpr::getType()
{
    return FLOAT;
}

Type GTExpr:: getType(){
    return BOOL;
}

Type LTExpr:: getType(){
    return BOOL;
}
