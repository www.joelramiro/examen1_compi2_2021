%option noyywrap
%option yylineno

%{
    #include "tokens.h"
%}

DIGIT [0-9]
ID [a-zA-Z]([a-zA-Z0-9])* 

%%
{DIGIT}+"."{DIGIT}+ { yylval.float_t = atof(yytext); return TK_LIT_FLOAT; }
{DIGIT}+ { yylval.float_t = atof(yytext); return TK_LIT_FLOAT; }
"+" { return ADD; }
"=" { return '='; }
"-" { return SUB; }
"*" { return MUL; }
"/" { return DIV; }
"(" { return '('; }
")" { return ')'; }
"," { return ','; }
";" { return ';'; }
">" { return '>'; }
"<" { return '<'; }
"let" { return LET; }
"while" { return WHILE; }
"do" { return DO; }
"done" { return DONE; }
{ID} {yylval.string_t = yytext; return TK_ID; }
[\n\r]+ { return EOL; }
[\n] { return EOL; }
[ \t] { /* ignorar */ }
. { printf("invalido %c\n", yytext[0]); }
%%