%code requires{
    #include "ast.h"
}

%{

    #include <cstdio>
    using namespace std;
    int yylex();
    extern int yylineno;
    void yyerror(const char * s){
        fprintf(stderr, "Line: %d, error: %s\n", yylineno, s);
    }

    #define YYERROR_VERBOSE 1
%}

%union{
    const char * string_t;
    float float_t;

    Expr * expr_t;
    Statement * statement_t;
    StatementList * statement_list_t;
    ArgumentList * argument_list_t;
    ParameterList * parameter_list_t;

}

%token<float_t> TK_LIT_FLOAT
%token<string_t> TK_ID
%type<argument_list_t> argument_expression_list
%type<parameter_list_t> parameters_list
%type<statement_list_t> statement_list start
%type<expr_t> number additive_expression multiplicative_expression relational_expression 
%type<expr_t> primary_expression expression assignment_expression


%token EOL
%token ADD SUB MUL DIV IF THEN ENDIF WHILE DO DONE ELSE LET

%type<statement_t> method_definition while_statement call_method expression_statement block_statement statement variable_definition main_logical


%%

init: start 
{
    list<Statement* >::iterator it = $1->begin();
    while(it != $1->end()){
        printf("result: %.6f \n", (*it)->evaluate());
        it++;
    }

}
    ;
start: start main_logical {$$ = $1; $$->push_back($2);}
      | start EOL {$$ = $1;}
      | main_logical {$$ = new StatementList; $$->push_back($1);}
     ;

main_logical: statement {$$ = $1; }
            | method_definition {$$ = $1; }
            ;

variable_definition: LET TK_ID '=' expression { new VariableStatement($2,$4,yylineno); }
                   ;

statement:  call_method {$$ = $1; }
          | expression_statement { $$ = $1; }
          | while_statement {$$ = $1;}
          | variable_definition {$$ = $1;}
         ;

call_method: TK_ID '(' argument_expression_list ')' { $$ = new CallStatement($1,*$3,yylineno);}
           | TK_ID '(' ')' { $$ = new CallStatement($1,*(new ArgumentList),yylineno);}
           ;

argument_expression_list: argument_expression_list ',' primary_expression {$$ = $1;  $$->push_back($3);}
         | primary_expression { $$ = new ArgumentList; $$->push_back($1);}
         ;

while_statement: WHILE '(' expression_statement ')' DO block_statement DONE {  $$ = new WhileStatement($3,$6,yylineno);}
            ;
    
expression_statement: expression {$$ = new ExprStatement($1, yylineno);}
                    ;

method_definition: LET TK_ID '(' parameters_list ')' '=' block_statement {$$ = new MethodDefinition($2,*$4,$7,yylineno);} 
                  | LET TK_ID '(' ')' '=' block_statement   {$$ = new MethodDefinition($2,*(new ParameterList()),$6,yylineno);} 
                  ;

block_statement:  statement_list { 
                    $$ = new BlockStatement(*$1, yylineno);
               }
               ;

statement_list:statement_list ';' statement { $$ = $1; $$->push_back($3); }
              | statement { $$ = new StatementList; $$->push_back($1); }
              ;

parameters_list: parameters_list ',' TK_ID {$$ = $1; Parameter* p = new Parameter($3,yylineno); $$->push_back(p);}
               | TK_ID { $$ = new ParameterList; Parameter* p = new Parameter($1,yylineno); $$->push_back(p); }
               ;


//EXPRESIONES

expression: assignment_expression {$$ = $1;}
          ;


assignment_expression: primary_expression '=' assignment_expression {$$ = new AssignExpr($1,$3,yylineno);}
                     | relational_expression {$$ = $1;}
                     ;

relational_expression: relational_expression '>' additive_expression { $$ = new GTExpr($1, $3, yylineno); }
                     | relational_expression '<'   additive_expression  { $$ = new LTExpr($1, $3, yylineno); }
                     | additive_expression {$$ = $1;}
                     ;

                     
additive_expression:  additive_expression ADD multiplicative_expression { $$ = new AddExpr($1, $3, yylineno); }
                    | additive_expression SUB multiplicative_expression { $$ = new SubExpr($1, $3, yylineno); }
                    | multiplicative_expression {$$ = $1;}
                    ;

multiplicative_expression: multiplicative_expression MUL primary_expression { $$ = new MulExpr($1, $3, yylineno); }
      | multiplicative_expression DIV primary_expression { $$ = new DivExpr($1, $3, yylineno); }
      | primary_expression {$$ = $1;}
      ;

primary_expression: TK_ID {$$ = new IdExpr($1, yylineno);}
    | number {$$ = $1;}
    ;

number: TK_LIT_FLOAT { $$ = new FloatExpr($1 , yylineno);}
      ;

%%
